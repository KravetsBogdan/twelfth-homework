let btnList = document.querySelectorAll('.btn')
document.addEventListener('keydown', (e) => {
    for (let btn of btnList) {
        btn.classList.remove('blue')
        if (btn.dataset.key === e.key) {
            btn.classList.add('blue')
        }
    }
})
